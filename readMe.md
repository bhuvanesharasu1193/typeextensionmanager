#######################
Type Extension Manager
#######################

Usage:
------
1. Clone this repo into gtnexus/platform
2. Do npm install or sudo npm install(not recommanded)
3. Initiate the script by typing `npm start`
4. Follow the guide to have type extension and axus setup for you

NOTE:
Supported docs PO INV PL PP

Document can be fetched and setup in your customer/module/resources folder if you provide the script with ReferenceNumber(PO Number, PL#,PP#)

When you run this for the first time it would require you to key in SUPQ details such as username, password(encrypted and stored) and dataKey. If you make a mistake while doing this kindly delete the module.ini file. ReRun the script after first run.