/**
 * Type Extension Manager
 * Developer(s): Bhuvanesh(bhuvanesh.arasu@infor.com)	
 */

import readline from 'readline';
import jake from 'jake';
import fs from 'fs';
import path from 'path';
import needle from 'needle';
import moment from 'moment'

const Cryptr = require('cryptr');
const cryptr = new Cryptr('gtnexusisBest@123');

let essentials = {};
let writeOut = {};

const task = {jake : task};

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });


const DOCUMENTS = {};

let initModules = ()=>{
  return new Promise((resolve, reject) => {
    createFile("module.ini", "./").then(fs.readFile('./module.ini','utf8', (err, data) => {
      if(err){
        console.log("Error reading data");
        essentials["existing"] = false;
        // reject();
      }else{
        essentials = JSON.parse(data);
        essentials["existing"] = true;
      }
      resolve();
    }))
  });
}

let getPlatformLocation = ()=>{
  return new Promise((resolve, reject)=>{
    essentials["pwd"] = __dirname;
    let platformPath = essentials["pwd"].split(path.sep);
    essentials["customerDirectory"] = `/${path.join(platformPath[0],platformPath[1],platformPath[2],platformPath[3],"customer")}`;
    essentials["customerTestDirectory"] = `/${path.join(platformPath[0],platformPath[1],platformPath[2],platformPath[3], "test","customer")}`;
    if(!essentials.existing){
      writeOut = {
        file:{
          pwd: `${essentials.pwd}`,
          customerDirectory: `${essentials.customerDirectory}`,
          customerTestDirectory: `${essentials.customerTestDirectory}`
        }
      }
    }
    resolve();
  });
}
let getUserCredentials = ()=>{
  return new Promise((resolve, reject) => {
    rl.question(`Enter your SUPORTQ Credentials(Need this to fetch data and create sample files for your module)\nUserName:\n`, (username) => {
        if(username){
            essentials["credentials"] = {};
            console.log("Password:");
            rl.stdoutMuted = true;
            rl.question(``, (password) => {
              rl.stdoutMuted = false;
              console.log("Datakey:");
              rl.stdoutMuted = true;
              essentials.credentials["username"] = username;
              essentials.credentials["password"] = cryptr.encrypt(password); 
              console.log("Data Key:");
              rl.question(``, (dataKey) =>{
                if(dataKey){
                  essentials.credentials["dataKey"] = dataKey;
                  writeOut.user = essentials.credentials;                        
                  resolve();        
                }
              });           
            });
        }
    });
})
}

let getCustomerName = ()=>{
  return new Promise((resolve, reject) => {
    rl.question(`Enter Customer Name:`, (customerName) => {
      if(customerName){
        essentials["customer"] = {};
        essentials.customer["customerName"] = customerName;
        resolve();
      }else{
        reject();
      }  
    })
  })
}

let ruleSetType = ()=>{

}

let fileName = ()=>{

}

let crateConfigFile = ()=>{

}

let getDocumentType = () =>{
  return new Promise((resolve, reject) =>{
    rl.question(`Document Type - PO|PL|PP|INV|Custom Document Name:`, (documentType) => {
      let docType = null;
      let shortForm = null;
      if(documentType){
          switch(documentType.toLowerCase()){
              case 'po':
              case 'order':
                  docType = `PurchaseOrder`
                  shortForm = `PO`
                  break;
              case 'inv':
              case 'invoice':
                  docType = `CommercialInvoice`
                  shortForm = `INV`
                  break;
              case 'pl':
              case 'pkm':
              case 'packinglist':
                  docType = `PackingManifest`    
                  shortForm = `PL`
                  break;
              case 'pp':
              case 'packingplan':
              case 'packingplanmanifest':
                  docType = `PackingPlan`
                  shortForm = `PP`
                  break;
              default:
                  docType = `Document`    
                  shortForm = `DOC`
                  break;
          }
          essentials.customer.documentType = docType;
          essentials.customer.docShortForm = shortForm;
          resolve();
      }else{
          throw new Error('Please provide the document type[PO|PP|PL|INV|PTS|CustomName]')
      }
  })
  })
}

let getRuleSetType = () =>{
  return new Promise((resolve, reject) => {
    rl.question(`Type of ruleset:(Validation|Population) - pop,vld,pops,vlds,populations,validations ?`, (rulesetName) =>{
        if(rulesetName){
            let type = null;
            switch(rulesetName.toLowerCase()){
                case 'validation':
                case 'validations':
                case 'vlds':
                case 'vld':
                    type = essentials.customer.customerName+essentials.customer.documentType+"ValidationsRuleSet"
                    break;
                case 'population':
                case 'populations':
                case 'pops':
                case 'pop':
                    type = essentials.customer.customerName+essentials.customer.documentType+"PopulationsRuleSet"
                    break;
                default:
                    type = essentials.customer.customerName+essentials.customer.documentType+"RuleSet"    
                    break;                                                
            }
            essentials.customer['ruleSetType'] = type;
            resolve();
        }else{
            throw new Error('Please provide the type of TypeExtension [Population|Validation]')
        }
    });
})
}

let createCustomerFile = () =>{
  return new Promise((resolve, reject) =>{
    createFile(`${essentials.customer.ruleSetType}.js`, `${essentials.file.customerDirectory}/${essentials.customer.customerName}/${essentials.customer.documentType}/${essentials.customer.ruleSetType}/`).then(()=>{
      createFile(`${essentials.customer.ruleSetType}.spec.js`, `${essentials.file.customerTestDirectory}/${essentials.customer.customerName}/${essentials.customer.documentType}/${essentials.customer.ruleSetType}/`).then(()=>{
        createFile(`${essentials.customer.documentType}.json`,`${essentials.file.customerTestDirectory}/${essentials.customer.customerName}/${essentials.customer.documentType}/${essentials.customer.ruleSetType}/resources/`)
        resolve()
      }).catch(()=>{
        reject();
      });
    }).catch(()=>{
      reject();
    });
  })  
}

let createCustomerDirectories = () =>{
  return new Promise((resolve, reject) => {
    if(essentials.customer.customerName){
      const customerName = essentials.customer.customerName;
      createFolder(customerName, essentials.file.customerDirectory).then(()=>{
        createFolder(customerName, essentials.file.customerTestDirectory).then(() =>{
          createFolder(essentials.customer.ruleSetType, `${essentials.file.customerDirectory}/${essentials.customer.customerName}/${essentials.customer.documentType}`);
          createFolder(essentials.customer.ruleSetType, `${essentials.file.customerTestDirectory}/${essentials.customer.customerName}/${essentials.customer.documentType}`);
          createFolder("resources", `${essentials.file.customerTestDirectory}/${essentials.customer.customerName}/${essentials.customer.documentType}/${essentials.customer.ruleSetType}`);
          essentials.customer['customerPlatformPath'] = `${essentials.file.customerDirectory}/${essentials.customer.customerName}/${essentials.customer.documentType}/${essentials.customer.ruleSetType}`
          essentials.customer['customerPlantformTestPath'] = `${essentials.file.customerTestDirectory}/${essentials.customer.customerName}/${essentials.customer.documentType}/${essentials.customer.ruleSetType}`
          resolve();
        }).catch(()=>{
          reject();
        })
      }).catch(()=>{
        reject();
      });
    }else{
      reject();
    }
  })
}

let getJIRANumber = () =>{
  return new Promise((resolve, reject) => {
    rl.question(`Please enter your JIRA#:`, (jiraNumber) => {
      let printJiraNumber = null;
      if(jiraNumber && jiraNumber != ``){
        console.log(jiraNumber.includes("PSO-"));
        if(jiraNumber.includes("PSO-")){
          printJiraNumber = jiraNumber
        }else{
          printJiraNumber =`PSO-${jiraNumber}`
        }
      }else{
        printJiraNumber+=`PSO-CHANGEME`
      }
      essentials.customer.jiraNumber = printJiraNumber
      resolve();
    })
  })
}

let getSampleData = () =>{
  return new Promise((resolve, reject) =>{
    rl.question(`Would you want me to setup sample data for you?[Y|N]`, (isDataRequired) => {
      essentials["environment"] = "https://network-supportq.qa.gtnexus.com/rest/3.1/";
      if(isDataRequired.toLowerCase() === "y"){
        let docType = essentials.customer.documentType;
        let reqDoc = "";
        let options = {
          headers:{
            Authorization: `${essentials.user.Authorization}`,
            dataKey: `${essentials.user.dataKey}`
          }
        }
        rl.question(`Sample Ref#:`, (sampleRefNumber)=>{
          if(sampleRefNumber){
            let url = "";
            let baseUrl = "";
            switch(docType.toLowerCase()){
              case 'po':
              case 'purchaseorder':
              case 'order':
                  reqDoc = `OrderDetail/query?oql=poNumber=`
                  url+=`${essentials.environment}/${reqDoc}"${sampleRefNumber}"`
                  baseUrl+=`${essentials.environment}/OrderDetail/`
                  essentials.customer.sampleRefNumber = sampleRefNumber;
                  break;
              case 'pl':
              case 'pkm':
              case 'packingmanifest':  
                  reqDoc = `PackingListDetail/`
                  url+=`${essentials.environment}/${reqDoc}${sampleRefNumber}`
                  baseUrl+=`${essentials.environment}/${reqDoc}`
                  essentials.customer.sampleRefNumber = sampleRefNumber;
                  break;
              case 'pp':
              case 'packingplan':
              case 'packingplanmanifest':
                  reqDoc = `PackingPlanDetail/`
                  url+=`${essentials.environment}/${reqDoc}${sampleRefNumber}`
                  baseUrl+=`${essentials.environment}/${reqDoc}`
                  essentials.customer.sampleRefNumber = sampleRefNumber;
                  break;
              case 'inv':
              case 'invoice':
              case 'commercialinvoice':  
                  reqDoc = `InvoiceDetail/query?oql=invoiceNumber=`
                  url+=`${essentials.environment}/${reqDoc}"${sampleRefNumber}"`
                  baseUrl+=`${essentials.environment}/InvoiceDetail/`
                  essentials.customer.sampleRefNumber = sampleRefNumber;
                  break;
              default:
                  break;
            }
            console.log("HELLO WORLD"+url);
            fs.readFile(`${essentials.customer.customerPlantformTestPath}/resources/${essentials.customer.documentType}.json`,'utf8', (err, existingData) =>{
              if(err || !existingData){
                needle('get', url, options).then((res)=>{
                  if(res.body){
                    if(res.body.subMessageId){
                      setupResources(res.body);
                      setupAxus();
                    }else{
                      prepareData(res.body, baseUrl).then((fineData) =>{
                        setupResources(fineData);
                        setupAxus();
                      }).catch()

                    }                    
                    resolve();
                  }else{
                    reject();
                  }
                })
                .catch((err)=>{
                  console.log(err)
                  reject();
                })
              }else{
                reject();
              }
            })
            fs.readFile(`${essentials.customer.customerPlatformPath}/${essentials.customer.ruleSetType}.js`,'utf8', (err, existingData) =>{
              if(err || !existingData){
                setupTypeExtension();
                resolve();
              }else{
                reject();
              }
            })
          }
        });
      }else{
        setupResources({});
        setupAxus();
        setupTypeExtension();
        resolve();
      }
    })
  })
}

let prepareData = (data, baseUrl) =>{
  return new Promise((resolve, reject) => {
    let requiredFullData = ['purchaseorder', 'commercialinvoice']
    let objectUidList = {
      purchaseorder:"orderUid",
      commercialinvoice: "invoiceUid"
    }
    let docType = essentials.customer.documentType;
    console.log(baseUrl);
    if(requiredFullData.indexOf(docType.toLowerCase()) > -1){
      let objectUid = data.result ? data.result[0][objectUidList[docType.toLowerCase()]]:null;
      console.log(objectUid);
      if(objectUid){
        needle('get', baseUrl+objectUid, {
          headers:{
            Authorization: `${essentials.user.Authorization}`,
            dataKey: `${essentials.user.dataKey}`
          }
        }).then((res)=>{
          console.log(res.body)
          resolve(res.body)
        }).catch((err)=>{
          reject(null);
        })
      }
    }else{
      resolve(data)
    }
  })
}

let setupResources = (data) =>{
  writeDateToFiles(`${essentials.customer.customerPlantformTestPath}/resources/${essentials.customer.documentType}.json`,data)
}

let setupAxus = () =>{
  let jiraNumber = `${essentials.customer.jiraNumber}`
  let date = `${moment().format("MM/DD/YYYY")}`
  let who = essentials.user.username.charAt(0).toUpperCase()+essentials.user.username.charAt(1).toUpperCase()||`¯\\_(ツ)_/¯`
  let description = `UPDATE THE DESCRIPTION`
  let sampleCode = `
/**
 *   C H A N G E    L  O G
 *
 *  (B)ug/(E)nh/(I)DB #    Date      Who  Description
 *  -------------------  ----------  ---  ---------------------------------------------------------------
 *	${jiraNumber}\t\t\t\t\t\t ${date} ${who}\t${description}
 */
  let chai = require('chai');
  chai.use(require('chai-things'));
  let expect = chai.expect;
  let axus = require('axus');
  let ctx = axus
  .requireLocal('../customer/${essentials.customer.customerName}/${essentials.customer.documentType}/${essentials.customer.ruleSetType}', undefined, {
    console: console
  })
  .seed(); //ADD YOUR SEEDFILE HERE
  describe('${essentials.customer.customerName} ${essentials.customer.ruleSetType}:', () => {
    beforeEach(() => {
      ctx.Providers.reset();
    });
  let ${(essentials.customer.docShortForm)}_Sample_${essentials.customer.sampleRefNumber||""} = require('./resources/${essentials.customer.documentType}.json');
  describe('${essentials.customer.ruleSetType}.1:', () => {
    it('Positive: ${essentials.customer.customerName} ', (done) => {
      //TODO
      ctx.fnOnEvent(${(essentials.customer.docShortForm)}_Sample_${essentials.customer.sampleRefNumber});
      done();
    });
  });
});
  `
  fs.writeFileSync(`${essentials.customer.customerPlantformTestPath}/${essentials.customer.ruleSetType}.spec.js`, '\uFEFF'+sampleCode.replace(/\n/g, '\r\n'))
}

let setupTypeExtension = () => {
  let jiraNumber = `${essentials.customer.jiraNumber}`
  let date = `${moment().format("MM/DD/YYYY")}`
  let who = essentials.user.username.charAt(0).toUpperCase()+essentials.user.username.charAt(1).toUpperCase()||`¯\\_(ツ)_/¯`
  let description = `UPDATE THE DESCRIPTION`
  let sampleCode = `
  /**
   *   C H A N G E    L  O G
   *
   *  (B)ug/(E)nh/(I)DB #    Date      Who  Description
   *  -------------------  ----------  ---  ---------------------------------------------------------------
   *	${jiraNumber}\t\t\t\t\t\t ${date}\t\t${who}\t${description}
   */
    function fnOnEvent(${(essentials.customer.docShortForm).toLowerCase()}){
      /**
       * THIS IS AUTO GENERATED 
       */
      console.log(${(essentials.customer.docShortForm).toLowerCase()})
    }
    `
    fs.writeFileSync(`${essentials.customer.customerPlatformPath}/${essentials.customer.ruleSetType}.js`, '\uFEFF'+sampleCode.replace(/\n/g, '\r\n'))
}

let writeDateToFiles = (path ,data) =>{
  fs.writeFile(path, JSON.stringify(data, null, 4), 'utf8')
}

let getBasicAuth = () =>{
  return new Promise((resolve, reject) => {
    let buff = new Buffer(essentials.user.username+":"+cryptr.decrypt(essentials.user.password))
    if(buff){
      essentials.user.Authorization = `Basic ${buff.toString('base64')}`
      resolve()
    }else{
      reject()
    }
  })
}

let createFile = (fileName, filePath) =>{
  console.log(`touch ${filePath}${fileName}`);
  return new Promise((resolve, reject) => {
    if(fileName && filePath){
      jake.exec(`touch ${filePath}${fileName}`);
      resolve();
    }else{
      reject();
    }
  });
}

let createFolder = (folderName, folderPath) => {
  return new Promise((resolve, reject) => {
    if(folderName && folderPath){
      jake.mkdirP(`${folderPath}/${folderName}`);
      resolve();
    }else{
      reject();
    }
  })
}

let createConfigFile = () => {
  jake.exec('touch module.ini');
}

rl._writeToOutput = function _writeToOutput(stringToWrite) {
  if (rl.stdoutMuted)
    rl.output.write("*");
  else
    rl.output.write(stringToWrite);
};

let processCustomer = async () =>{
  await getCustomerName();
  await getDocumentType();
  await getRuleSetType();
  await createCustomerDirectories();
  await createCustomerFile();
  await getJIRANumber();
  await getSampleData();
}


const main = async () => {
    await initModules();
    if(!essentials.existing){
      await getPlatformLocation();
      await getUserCredentials();
      fs.writeFileSync('module.ini', JSON.stringify(writeOut), 'utf8');
    }
    await getBasicAuth();
    await processCustomer();
    console.log(essentials)
    rl.close()
}


main()